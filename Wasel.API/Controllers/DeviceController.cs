﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wasel.Core.IRepositories;
using Wasel.DAL.InputDTOs;
using Wasel.DAL.Models.WritableModels;

namespace Wasel.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeviceController : ControllerBase
    {
        //initializations
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<DeviceController> _logger;
        private readonly IMapper _mapper;

        //Constructor
        public DeviceController(IUnitOfWork unitOfWork, ILogger<DeviceController> logger, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddDeviceInfo([FromBody] DeviceInputDTO deviceDTO)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError($"Invalid POST attempt in {nameof(AddDeviceInfo)}");
                return BadRequest(ModelState);
            }

            var dto = _mapper.Map<DeviceInfo>(deviceDTO);
            await _unitOfWork.DeviceInfos.Insert(dto);
            await _unitOfWork.Save();

            return Ok(dto);
        }
    }
}
