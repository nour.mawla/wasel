﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Twilio;
using Twilio.Exceptions;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using Wasel.Core.IRepositories;
using Wasel.Core.OutputDTOs;
using Wasel.DAL.InputDTOs;
using Wasel.DAL.Models.WritableModels;

namespace Wasel.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TempOTPController : ControllerBase
    {
        //Initializations
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<TempOTPController> _logger;
        private readonly IMapper _mapper;

        //Constructor
        public TempOTPController(IUnitOfWork unitOfWork, ILogger<TempOTPController> logger, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
        }

        #region Generating OTP and SMS Sending
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GenerateOTP ([FromBody] TempOTPInputDTO tempOTPInputDTO)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError($"Invalid POST attempt in {nameof(GenerateOTP)}");
                return BadRequest(ModelState);
            }
            var otp = _mapper.Map<TempOTP>(tempOTPInputDTO);

            TempOTP x = await _unitOfWork.TempOTPs.Get(n => n.PhoneNumber == otp.PhoneNumber);
            
            if (x == null)
            {
                await _unitOfWork.TempOTPs.Insert(otp);
                await _unitOfWork.Save();
            }
            else
            {
                await _unitOfWork.TempOTPs.Delete(x.TempOTPId);
                await _unitOfWork.TempOTPs.Insert(otp);
                await _unitOfWork.Save();
            }

            return Ok("OTP generated successfully!" + "\n \n" + await SendSMS(otp.OTP, otp.PhoneNumber));
        }
        #region SMS Generation using Twilio
        private async Task <string> SendSMS(string oTP, string phoneNumber)
        {
            var accountSid = "AC903a7ea1b3d377acd14f28e12d7a90f9";
            var authToken = "2f35f3c10da5741ad7bdfcfbd8e72713";

            TwilioClient.Init(accountSid, authToken);

            var to = (phoneNumber.StartsWith("00") ? "+" + phoneNumber.Substring(2) : phoneNumber);
            var from = new PhoneNumber("+17063832827");
            var boby = "Your Wasel verification Code is: " + oTP;

            try
            {
                var message = await MessageResource.CreateAsync(
                    to: to,
                    from: from,
                    body: boby
                    );

                if (message.Status == MessageResource.StatusEnum.Failed)
                {
                    return $"Failed to send SMS to {phoneNumber}";
                }
                return $"Message sent to {phoneNumber} successfully!";
            }
            catch (ApiException e)
            {
                return e.Status + "\n" + e.ToString();
            }
        }
        #endregion

        #endregion

        #region Get OTP By Phone Number
        //[HttpGet("{phoneNumber}", Name = "GetOTP")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status500InternalServerError)]
        //public async Task<IActionResult> GetOTP(string phoneNumber)
        //{
        //    var otp = await _unitOfWork.TempOTPs.Get(q => q.PhoneNumber == phoneNumber);
        //    if (otp != null)
        //    {
        //        var result = _mapper.Map<TempOTPDTO>(otp);
        //        return Ok(result);
        //    }
        //    else
        //        return Ok("No OTP generated for this number");
        //}
        #endregion

        #region Verify OTP
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> VerifyOTP([FromBody] VerifyOTP verifyOTP)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError($"Invalid UPDATE attempt in {nameof(VerifyOTP)}");
                return BadRequest(ModelState);
            }

            var otp = await _unitOfWork.TempOTPs.Get(q => q.PhoneNumber == verifyOTP.PhoneNumber);

            if (otp == null)
            {
                _logger.LogError($"Invalid UPDATE attempt in {nameof(VerifyOTP)}");
                return BadRequest("Submitted data is invalid");
            }

            if (verifyOTP.OTP != otp.OTP)           
                return BadRequest("Submitted code is invalid");
           
            else if (DateTime.UtcNow > otp.ExpiryDate)           
                return BadRequest("You have exceeded the expiry date. Please try again");
            
            else
            {
                otp.IsValidated = true;
                _unitOfWork.TempOTPs.Update(otp);
                await _unitOfWork.Save();
                return Ok("Success");
            }              
        }
        #endregion
    }
}
