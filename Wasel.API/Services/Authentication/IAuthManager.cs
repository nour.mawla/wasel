﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wasel.DAL.InputDTOs;

namespace Wasel.API.Services.Authentication
{
    public interface IAuthManager
    {
        Task<bool> ValidateUser(LoginUserDTO userDTO);
        Task<string> CreateToken();
    }
}
