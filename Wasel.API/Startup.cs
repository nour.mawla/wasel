using AspNetCoreRateLimit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Wasel.API.Services.Authentication;
using Wasel.Core.Configurations.Mapper;
using Wasel.Core.IRepositories;
using Wasel.Core.Repositories;
using Wasel.DAL.DataAccessContext;

namespace Wasel.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Connecting to MySQL Server
            string mySqlConnectionStr = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContextPool<DBAccessContext>(options => options.UseMySql(mySqlConnectionStr, ServerVersion.AutoDetect(mySqlConnectionStr)));

            //Cors Configuration
            services.AddCors(o => {
                o.AddPolicy("AllowAll", builder =>
                    builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });

            //Configuring the Identity Model User
            services.ConfigureIdentity();

            //Enabling Adapters
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            //Enabling Automapping
            services.AddAutoMapper(typeof(MapperInitializer));

            //Enabling Token Generation
            services.ConfigureJWT(Configuration);

            //Enabling Authentication
            services.AddAuthentication();
            services.AddScoped<IAuthManager, AuthManager>();

            //Initial Caching Configuration
            services.AddControllers(config =>
            {
                config.CacheProfiles.Add("120SecondsDuration", new CacheProfile
                {
                    Duration = 120

                });
            }).AddNewtonsoftJson(op =>
            op.SerializerSettings.ReferenceLoopHandling =
                Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            //Configuring Cashing
            services.ConfigureHttpCacheHeaders();
            services.AddMemoryCache();

            //Configuring Rate Limit
            services.ConfigureRateLimiting();

            services.AddHttpContextAccessor();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Wasel", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //Enabling Swaggers
            app.UseSwagger();
            app.UseSwaggerUI(c => {
                string swaggerJsonBasePath = string.IsNullOrWhiteSpace(c.RoutePrefix) ? "." : "..";
                c.SwaggerEndpoint($"{swaggerJsonBasePath}/swagger/v1/swagger.json", "Wasel's API");
            });

            //Enabling Cors
            app.UseCors("AllowAll");

            //Enabling Authentication
            app.UseAuthentication();

            //Enabling Universal Error handling
            app.ConfigureExceptionHandler();

            //Enabling Caching
            app.UseResponseCaching();
            app.UseHttpCacheHeaders();

            //Enabling Rate Limit
            app.UseIpRateLimiting();

            //Defaults
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
