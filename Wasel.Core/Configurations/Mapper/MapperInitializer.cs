﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wasel.Core.OutputDTOs;
using Wasel.DAL.Models.StandardModels;
using Wasel.DAL.InputDTOs;
using Wasel.DAL.Models.WritableModels;

namespace Wasel.Core.Configurations.Mapper
{
    public class MapperInitializer : Profile
    {
        //AutoMap the models to their respective DTOs inside the Constructor 
        public MapperInitializer()
        {
            CreateMap<User, UserDTO>().ReverseMap();
            CreateMap<User, LoginUserDTO>().ReverseMap();

            CreateMap<TempOTP, TempOTPDTO>().ReverseMap();
            CreateMap<TempOTP, TempOTPInputDTO>().ReverseMap();
            CreateMap<TempOTP, VerifyOTP>().ReverseMap();

            CreateMap<DeviceInfo, DeviceInputDTO>().ReverseMap();

            //CreateMap<Hotel, HotelDTO>().ReverseMap();
            //CreateMap<Hotel, CreateHotelDTO>().ReverseMap();
            //CreateMap<Hotel, UpdateHotelDTO>().ReverseMap();
        }
    }
}
