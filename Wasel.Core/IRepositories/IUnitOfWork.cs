﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wasel.DAL.Models.WritableModels;

namespace Wasel.Core.IRepositories
{
    public interface IUnitOfWork
    {
        //Initializing the created Models
        IGenericRepository<TempOTP> TempOTPs { get; }
        IGenericRepository<DeviceInfo> DeviceInfos { get; }
        Task Save();
    }
}
