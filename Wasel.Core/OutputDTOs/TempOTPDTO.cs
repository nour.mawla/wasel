﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Wasel.DAL.InputDTOs;

namespace Wasel.Core.OutputDTOs
{
    public class TempOTPDTO
    {
        public string OTP { get; set; }

    }
}
