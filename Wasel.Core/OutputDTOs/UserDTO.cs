﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Wasel.DAL.InputDTOs;

namespace Wasel.Core.OutputDTOs
{
    public class UserDTO : LoginUserDTO
    {
        public UserDTO()
        {
            Roles = new List<string>() { "User" };
        }
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public ICollection<string> Roles { get; set; }
    }
}
