﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wasel.Core.IRepositories;
using Wasel.DAL.DataAccessContext;
using Wasel.DAL.Models.WritableModels;

namespace Wasel.Core.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DBAccessContext _context;

        //Calling the Repository of each Model Class:
        private IGenericRepository<TempOTP> _tempOTPs;
        private IGenericRepository<DeviceInfo> _deviceInfos;

        public UnitOfWork(DBAccessContext context)
        {
            _context = context;
        }
        //Implementing the Repository of each Model Class:
        public IGenericRepository<TempOTP> TempOTPs => _tempOTPs ??= new GenericRepository<TempOTP>(_context);

        public IGenericRepository<DeviceInfo> DeviceInfos => _deviceInfos ??= new GenericRepository<DeviceInfo>(_context);

        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }
        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }
    }
}
