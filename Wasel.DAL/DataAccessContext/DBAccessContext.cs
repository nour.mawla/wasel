﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Wasel.DAL.EntityConfiguration;
using Wasel.DAL.Models.WritableModels;

namespace Wasel.DAL.DataAccessContext
{
    public class DBAccessContext : IdentityDbContext <User, Role, int>
    {
        public DBAccessContext(DbContextOptions<DBAccessContext> options) : base(options)
        {

        }

        #region Writable Models Configurations
        public DbSet<TempOTP> TempOTPs { get; set; }
        public DbSet<DeviceInfo> DeviceInfos { get; set; }
        #endregion

        #region Identity Models configurations
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Customize the ASP.NET Identity model and override the defaults if needed.
            builder.Entity<User>(entity => { entity.ToTable(name: "Users"); });
            builder.Entity<Role>(entity => { entity.ToTable(name: "Roles"); });
            builder.Entity<IdentityUserRole<int>>(entity => { entity.ToTable("UserRoles"); });
            builder.Entity<IdentityUserClaim<int>>(entity => { entity.ToTable("Claims"); });
            builder.Entity<IdentityUserLogin<int>>(entity => { entity.ToTable("Logins"); });
            builder.Entity<IdentityRoleClaim<int>>(entity => { entity.ToTable("RoleClaims"); });
            builder.Entity<IdentityUserToken<int>>(entity => { entity.ToTable("UserTokens"); });

            //Calling the Seeding
            builder.ApplyConfiguration(new RoleConfiguration());
        }
        #endregion
    }
}
