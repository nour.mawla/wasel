﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wasel.DAL.Models.WritableModels;

namespace Wasel.DAL.EntityConfiguration
{
    public class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.HasData(

                new Role
                {
                    Id = 1,
                    Name = "User",
                    NormalizedName = "USER"
                },
                new Role
                {
                    Id = 2,
                    Name = "Driver",
                    NormalizedName = "DRIVER"
                }
            );
        }

    }
}
