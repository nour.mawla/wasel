﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Wasel.DAL.InputDTOs
{
    public class DeviceInputDTO
    {
        [Required]
        public string DeviceID { get; set; }        
        [Required]
        public string OS { get; set; }        
        [Required]
        public string OSVersion { get; set; }
    }
}
