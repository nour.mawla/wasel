﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Wasel.DAL.InputDTOs
{
    public class TempOTPInputDTO
    {
        [Required]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
    }

    public class VerifyOTP : TempOTPInputDTO
    {
        [Required]
        public string OTP { get; set; }
    }
}
