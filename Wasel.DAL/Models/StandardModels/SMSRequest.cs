﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Wasel.DAL.Models.StandardModels
{
    public class SMSRequest
    {
        [Required]
        public string PhoneNumber { get; set; }
    }
}
