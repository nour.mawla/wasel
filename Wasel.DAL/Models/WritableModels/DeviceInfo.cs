﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wasel.DAL.Models.WritableModels
{
    public class DeviceInfo
    {
        public int DeviceInfoId { get; set; }
        public string DeviceId { get; set; }
        public string OS { get; set; }
        public string OSVersion { get; set; }
    }
}
