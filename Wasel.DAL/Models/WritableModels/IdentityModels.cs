﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Wasel.DAL.Models.WritableModels
{
    public class UserRole : IdentityUserRole<int>
    {
    }

    public class Login : IdentityUserLogin<int>
    {
    }

    public class Role : IdentityRole<int>
    {
        public Role() { }
        public Role(string name) { Name = name; }
    }

    public class Token : IdentityUserToken<int>
    {

    }
}
