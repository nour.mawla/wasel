﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wasel.DAL.Models.WritableModels
{
    public class TempOTP
    {
        public TempOTP()
        {
            var dt = DateTime.UtcNow;
            CreatedDate = dt.Date.AddHours(dt.Hour).AddMinutes(dt.Minute).AddSeconds(dt.Second);
            var dt2 = DateTime.UtcNow.AddMinutes(3);
            ExpiryDate = dt2.Date.AddHours(dt2.Hour).AddMinutes(dt2.Minute).AddSeconds(dt.Second);
            OTP = GenOTP();
        }
        public int TempOTPId { get; set; }
        public string OTP { get; set; }
        public DateTime CreatedDate { get; set; } 
        public DateTime ExpiryDate { get; set; }
        public string PhoneNumber { get; set; }

        public bool IsValidated { get; set; } = false;
        private string GenOTP()
        {
            string num = "0123456789";
            int getIndex, otpDigit = 6, len = num.Length;
            string finalDigit, otp = string.Empty;

            for (int i = 0; i < otpDigit; i++)
            {
                do
                {
                    getIndex = new Random().Next(0, len);
                    finalDigit = num.ToCharArray()[getIndex].ToString();
                } while (otp.IndexOf(finalDigit) != -1);
                otp += finalDigit;
            }
            return otp;
        }
    }

}
